<?php /*========================================
list
================================================*/ ?>
<div class="c-dev-title1">list</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list1</div>
<div class="l-container">
	<ul class="c-list1">
		<li>
			<div class="c-list1__box c-list1__box--new">
				<a href="">
					<div class="c-list1__img">
						<img src="http://placehold.jp/160x160.png" class="pc-only" alt="">
						<img src="http://placehold.jp/340x340.png" class="sp-only" alt="">
					</div>
					<p class="c-list1__txt">300円+税</p>
				</a>
			</div>
		</li>
		<li>
			<div class="c-list1__box c-list1__box--restock">
				<a href="">
					<div class="c-list1__img">
						<img src="http://placehold.jp/160x160.png" class="pc-only" alt="">
						<img src="http://placehold.jp/340x340.png" class="sp-only" alt="">
					</div>
					<p class="c-list1__txt">300円+税</p>
				</a>
			</div>
		</li>
		<li>
			<div class="c-list1__box c-list1__box--new">
				<a href="">
					<div class="c-list1__img">
						<img src="http://placehold.jp/160x160.png" class="pc-only" alt="">
						<img src="http://placehold.jp/340x340.png" class="sp-only" alt="">
					</div>
					<p class="c-list1__txt">300円+税</p>
				</a>
			</div>
		</li>
		<li>
			<div class="c-list1__box">
				<a href="">
					<div class="c-list1__img">
						<img src="http://placehold.jp/160x160.png" class="pc-only" alt="">
						<img src="http://placehold.jp/340x340.png" class="sp-only" alt="">
					</div>
					<p class="c-list1__txt">300円+税</p>
				</a>
			</div>
		</li>
		<li>
			<div class="c-list1__box">
				<a href="">
					<div class="c-list1__img">
						<img src="http://placehold.jp/160x160.png" class="pc-only" alt="">
						<img src="http://placehold.jp/340x340.png" class="sp-only" alt="">
					</div>
					<p class="c-list1__txt">300円+税</p>
				</a>
			</div>
		</li>
		<li>
			<div class="c-list1__box">
				<a href="">
					<div class="c-list1__img">
						<img src="http://placehold.jp/160x160.png" class="pc-only" alt="">
						<img src="http://placehold.jp/340x340.png" class="sp-only" alt="">
					</div>
					<p class="c-list1__txt">300円+税</p>
				</a>
			</div>
		</li>
	</ul>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list1 (no text + no tag)</div>
<div class="l-container">
	<ul class="c-list1 is-no-tag">
		<li>
			<div class="c-list1__box">
				<a href="">
					<div class="c-list1__img">
						<img src="http://placehold.jp/160x160.png" class="pc-only" alt="">
						<img src="http://placehold.jp/250x250.png" class="sp-only" alt="">
					</div>
				</a>
			</div>
		</li>
		<li>
			<div class="c-list1__box">
				<a href="">
					<div class="c-list1__img">
						<img src="http://placehold.jp/160x160.png" class="pc-only" alt="">
						<img src="http://placehold.jp/250x250.png" class="sp-only" alt="">
					</div>
				</a>
			</div>
		</li>
		<li>
			<div class="c-list1__box">
				<a href="">
					<div class="c-list1__img">
						<img src="http://placehold.jp/160x160.png" class="pc-only" alt="">
						<img src="http://placehold.jp/250x250.png" class="sp-only" alt="">
					</div>
				</a>
			</div>
		</li>
		<li>
			<div class="c-list1__box">
				<a href="">
					<div class="c-list1__img">
						<img src="http://placehold.jp/160x160.png" class="pc-only" alt="">
						<img src="http://placehold.jp/250x250.png" class="sp-only" alt="">
					</div>
				</a>
			</div>
		</li>
		<li>
			<div class="c-list1__box">
				<a href="">
					<div class="c-list1__img">
						<img src="http://placehold.jp/160x160.png" class="pc-only" alt="">
						<img src="http://placehold.jp/250x250.png" class="sp-only" alt="">
					</div>
				</a>
			</div>
		</li>
		<li>
			<div class="c-list1__box">
				<a href="">
					<div class="c-list1__img">
						<img src="http://placehold.jp/160x160.png" class="pc-only" alt="">
						<img src="http://placehold.jp/250x250.png" class="sp-only" alt="">
					</div>
				</a>
			</div>
		</li>
	</ul>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list2</div>
<div class="l-container">
	<ul class="c-list2">
		<li>
			<div class="c-list2__box">
				<a href="">
					<div class="c-list2__img"><img src="http://placehold.jp/250x130.png" alt=""></div>
					<div class="c-list2__time">
						<time>2019.01.01</time>
						<span>キャンペーン</span>
					</div>
					<p class="c-list2__txt">ダミーです。新商品画像をアップしました。ITEMページをご覧ください！</p>
				</a>
			</div>
		</li>
		<li>
			<div class="c-list2__box">
				<a href="">
					<div class="c-list2__img"><img src="http://placehold.jp/250x130.png" alt=""></div>
					<div class="c-list2__time">
						<time>2019.01.01</time>
						<span>今月の新商品</span>
					</div>
					<p class="c-list2__txt">ダミーです。新商品画像をアップしました。ITEMページをご覧ください！</p>
				</a>
			</div>
		</li>
		<li>
			<div class="c-list2__box">
				<a href="">
					<div class="c-list2__img"><img src="http://placehold.jp/250x130.png" alt=""></div>
					<div class="c-list2__time">
						<time>2019.01.01</time>
						<span>ショップ</span>
					</div>
					<p class="c-list2__txt">ダミーです。新商品画像をアップしました。ITEMページをご覧ください！</p>
				</a>
			</div>
		</li>
		<li>
			<div class="c-list2__box">
				<a href="">
					<div class="c-list2__img"><img src="http://placehold.jp/250x130.png" alt=""></div>
					<div class="c-list2__time">
						<time>2019.01.01</time>
						<span>お知らせ</span>
					</div>
					<p class="c-list2__txt">ダミーです。新商品画像をアップしました。ITEMページをご覧ください！</p>
				</a>
			</div>
		</li>
	</ul>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list2 (no tag)</div>
<div class="l-container">
	<ul class="c-list2">
		<li>
			<div class="c-list2__box">
				<a href="">
					<div class="c-list2__img"><img src="http://placehold.jp/250x130.png" alt=""></div>
					<div class="c-list2__time">
						<time>2019.01.01</time>
					</div>
					<p class="c-list2__txt">ダミーです。新商品画像をアップしました。ITEMページをご覧ください！</p>
				</a>
			</div>
		</li>
		<li>
			<div class="c-list2__box">
				<a href="">
					<div class="c-list2__img"><img src="http://placehold.jp/250x130.png" alt=""></div>
					<div class="c-list2__time">
						<time>2019.01.01</time>
					</div>
					<p class="c-list2__txt">ダミーです。新商品画像をアップしました。ITEMページをご覧ください！</p>
				</a>
			</div>
		</li>
		<li>
			<div class="c-list2__box">
				<a href="">
					<div class="c-list2__img"><img src="http://placehold.jp/250x130.png" alt=""></div>
					<div class="c-list2__time">
						<time>2019.01.01</time>
					</div>
					<p class="c-list2__txt">ダミーです。新商品画像をアップしました。ITEMページをご覧ください！</p>
				</a>
			</div>
		</li>
		<li>
			<div class="c-list2__box">
				<a href="">
					<div class="c-list2__img"><img src="http://placehold.jp/250x130.png" alt=""></div>
					<div class="c-list2__time">
						<time>2019.01.01</time>
					</div>
					<p class="c-list2__txt">ダミーです。新商品画像をアップしました。ITEMページをご覧ください！</p>
				</a>
			</div>
		</li>
	</ul>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list3</div>
<div class="l-container">
	<ul class="c-list3">
		<li>
			<a href="">
				<img src="/assets/img/common/cou-banner-pc-1.jpg" width="200" class="pc-only" alt="">
				<img src="/assets/img/common/cou-banner-sp-1.jpg" class="sp-only" alt="">
			</a>
		</li>
		<li>
			<a href="">
				<img src="/assets/img/common/cou-banner-pc-2.jpg" width="200" class="pc-only" alt="">
				<img src="/assets/img/common/cou-banner-sp-2.jpg" class="sp-only" alt="">
			</a>
		</li>
		<li>
			<a href="">
				<img src="/assets/img/common/cou-banner-pc-3.jpg" width="200" class="pc-only" alt="">
				<img src="/assets/img/common/cou-banner-sp-3.jpg" class="sp-only" alt="">
			</a>
		</li>
		<li>
			<a href="">
				<img src="/assets/img/common/cou-banner-pc-4.jpg" width="200" class="pc-only" alt="">
				<img src="/assets/img/common/cou-banner-sp-4.jpg" class="sp-only" alt="">
			</a>
		</li>
		<li>
			<a href="">
				<img src="/assets/img/common/cou-banner-pc-5.jpg" width="200" class="pc-only" alt="">
				<img src="/assets/img/common/cou-banner-sp-5.jpg" class="sp-only" alt="">
			</a>
		</li>
	</ul>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list4</div>
<div class="l-main" style="margin: 0 auto">
	<ul class="c-list4">
		<li>
			<a href="">
				<div class="c-list4__box c-list4__box--new">
					<div class="c-list4__img">
						<img src="http://placehold.jp/240x240.png" class="pc-only" alt="">
						<img src="http://placehold.jp/340x340.png" class="sp-only" alt="">
					</div>
					<p class="c-list4__txt">ダミーです・商品名が入ります・ライフモコルシリーズ（巾着・ポーチ）</p>
					<p class="c-list4__price">300円+税</p>
				</div>
			</a>
		</li>
		<li>
			<a href="">
				<div class="c-list4__box c-list4__box--restock">
					<div class="c-list4__img">
						<img src="http://placehold.jp/240x240.png" class="pc-only" alt="">
						<img src="http://placehold.jp/340x340.png" class="sp-only" alt="">
					</div>
					<p class="c-list4__txt">ダミーです・商品名が入ります・ライフモコルシリーズ（巾着・ポーチ）</p>
					<p class="c-list4__price">300円+税</p>
				</div>
			</a>
		</li>
		<li>
			<a href="">
				<div class="c-list4__box c-list4__box--new">
					<div class="c-list4__img">
						<img src="http://placehold.jp/240x240.png" class="pc-only" alt="">
						<img src="http://placehold.jp/340x340.png" class="sp-only" alt="">
					</div>
					<p class="c-list4__txt">ダミーです・商品名が入ります・ライフモコルシリーズ（巾着・ポーチ）</p>
					<p class="c-list4__price">300円+税</p>
				</div>
			</a>
		</li>
		<li>
			<a href="">
				<div class="c-list4__box">
					<div class="c-list4__img">
						<img src="http://placehold.jp/240x240.png" class="pc-only" alt="">
						<img src="http://placehold.jp/340x340.png" class="sp-only" alt="">
					</div>
					<p class="c-list4__txt">ダミーです・商品名が入ります・ライフモコルシリーズ（巾着・ポーチ）</p>
					<p class="c-list4__price">300円+税</p>
				</div>
			</a>
		</li>
		<li>
			<a href="">
				<div class="c-list4__box">
					<div class="c-list4__img">
						<img src="http://placehold.jp/240x240.png" class="pc-only" alt="">
						<img src="http://placehold.jp/340x340.png" class="sp-only" alt="">
					</div>
					<p class="c-list4__txt">ダミーです・商品名が入ります・ライフモコルシリーズ（巾着・ポーチ）</p>
					<p class="c-list4__price">300円+税</p>
				</div>
			</a>
		</li>
		<li>
			<a href="">
				<div class="c-list4__box">
					<div class="c-list4__img">
						<img src="http://placehold.jp/240x240.png" class="pc-only" alt="">
						<img src="http://placehold.jp/340x340.png" class="sp-only" alt="">
					</div>
					<p class="c-list4__txt">ダミーです・商品名が入ります・ライフモコルシリーズ（巾着・ポーチ）</p>
					<p class="c-list4__price">300円+税</p>
				</div>
			</a>
		</li>
	</ul>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list5</div>
<div class="l-main" style="margin: 0 auto">
	<ul class="c-list5">
		<li class="c-list5__item">
			<div class="c-list5__wrap">
				<div class="c-list5__region"><p>神奈川</p></div>
				<div class="c-list5__store">
					<a href="" class="c-list5__link">トレッサ横浜店</a>
					<a href="" class="c-list5__link">ノースポート・モール店</a>
					<a href="" class="c-list5__link">新百合丘オーパ店</a>
					<a href="" class="c-list5__link">湘南藤沢オーパ店</a>
					<a href="" class="c-list5__link">ウィング上大岡店</a>
				</div>
			</div>
		</li>
	</ul>
</div>