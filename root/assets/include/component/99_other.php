<?php /*========================================
other
================================================*/ ?>
<div class="c-dev-title1">other</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-time1</div>
<div class="c-time1">
	<time>2019.01.01</time>
	<div class="c-tag1 c-tag1--c1">
		<span>キャンペーン</span>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-tag1</div>
<div class="c-tag1">
	<span>キャンペーン</span>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-tag1 c-tag1--c1</div>
<div class="c-tag1 c-tag1--c1">
	<span>キャンペーン</span>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-box1</div>
<div class="l-container">
	<div class="c-box1">
		<div class="c-box1__question is-active">
			<div class="c-box1__img"><img src="/assets/img/common/cou-img-19.png" width="25" alt=""></div>
			<p class="c-box1__txt">「クレジットカード」は利用できますか？</p>
		</div>
		<div class="c-box1__answer is-openfaq">
			<div class="c-box1__img"><img src="/assets/img/common/cou-img-20.png" width="25" alt=""></div>
			<p class="c-box1__txt">はい、ご利用可能でございます。<br />店舗により、お取扱い可能なカード会社が異なりますので直接お問い合わせ下さいませ。</p>
		</div>
	</div>
	<div class="c-box1">
		<div class="c-box1__question">
			<div class="c-box1__img"><img src="/assets/img/common/cou-img-19.png" width="25" alt=""></div>
			<p class="c-box1__txt">お店で買った商品を、家まで送ってもらうことは可能ですか？</p>
		</div>
		<div class="c-box1__answer">
			<div class="c-box1__img"><img src="/assets/img/common/cou-img-20.png" width="25" alt=""></div>
			<p class="c-box1__txt">はい、ご利用可能でございます。<br />店舗により、お取扱い可能なカード会社が異なりますので直接お問い合わせ下さいませ。</p>
		</div>
	</div>
	<div class="c-box1">
		<div class="c-box1__question">
			<div class="c-box1__img"><img src="/assets/img/common/cou-img-19.png" width="25" alt=""></div>
			<p class="c-box1__txt">お店で買った商品を、家まで送ってもらうことは可能ですか？</p>
		</div>
		<div class="c-box1__answer">
			<div class="c-box1__img"><img src="/assets/img/common/cou-img-20.png" width="25" alt=""></div>
			<p class="c-box1__txt">はい、ご利用可能でございます。<br />店舗により、お取扱い可能なカード会社が異なりますので直接お問い合わせ下さいませ。</p>
		</div>
	</div>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-box2</div>
<div class="l-container">
	<div class="c-box2">
		<div class="c-box2__ttl">
			<span>友だち特典 ①<img src="/assets/img/common/cou-img-15.png" width="117" alt=""></span>
		</div>
		<p class="c-box2__txt1">お誕生月は何度でも「8％OFF」！</p>
		<div class="c-box2__img">
			<img src="/assets/img/common/cou-img-17.png" width="250" alt="">
		</div>
		<div class="c-box2__txt2">
			<p>LINEのトーク画面と、誕生月が確認できるもの（免許証、保険証、学生証など）をレジにてご提示いただくと、誕生月は何回でも8％OFFでお買い物いただけます。</p>
		</div>
	</div>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-box3</div>
<div class="l-container">
	<div class="c-box3">
		<div class="c-box3__ttl">
			<p>新商品情報を受け取りたい！</p>
		</div>
		<div class="c-box3__imgText">
			<div class="c-box3__txt1">
				<p>アカウント名</p>
				<span>CouCou</span>
			</div>
			<div class="c-box3__imgText-img">
				<img src="/assets/img/common/cou-img-16.png" width="80" alt="">
			</div>
		</div>
		<div class="c-box3__txt2">
			<p>上記QRコードを読み取るか、LINEアプリを立ち上げ、「友だち」からアカウント名「CouCou」を検索して「友だち追加」を行ってください。</p>
		</div>
	</div>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-thumbnail1</div>
<div class="l-main" style="margin: 0 auto;">
	<div class="c-thumbnail1">
		<div class="c-thumbnail1__wrap">
			<div class="c-thumbnail1__img1" style="background-image: url('http://placehold.jp/500x500.png');"></div>
			<div class="c-thumbnail1__gallery">
				<a href="#" class="selected c-thumbnail1__link" data-img="http://placehold.jp/600x600.png">
					<div class="c-thumbnail1__img2" style="background-image: url('http://placehold.jp/600x600.png')"></div>
				</a>
				<a href="#" class="selected c-thumbnail1__link" data-img="http://placehold.jp/500x500.png">
					<div class="c-thumbnail1__img2" style="background-image: url('http://placehold.jp/500x500.png')"></div>
				</a>
				<a href="#" class="selected c-thumbnail1__link" data-img="http://placehold.jp/500x500.png">
					<div class="c-thumbnail1__img2" style="background-image: url('http://placehold.jp/500x500.png')"></div>
				</a>
				<a href="#" class="selected c-thumbnail1__link" data-img="http://placehold.jp/500x500.png">
					<div class="c-thumbnail1__img2" style="background-image: url('http://placehold.jp/500x500.png')"></div>
				</a>
				<a href="#" class="selected c-thumbnail1__link" data-img="http://placehold.jp/500x500.png">
					<div class="c-thumbnail1__img2" style="background-image: url('http://placehold.jp/500x500.png')"></div>
				</a>
				<a href="#" class="selected c-thumbnail1__link" data-img="http://placehold.jp/500x500.png">
					<div class="c-thumbnail1__img2" style="background-image: url('http://placehold.jp/500x500.png')"></div>
				</a>
				<a href="#" class="selected c-thumbnail1__link" data-img="http://placehold.jp/500x500.png">
					<div class="c-thumbnail1__img2" style="background-image: url('http://placehold.jp/500x500.png')"></div>
				</a>
				<a href="#" class="selected c-thumbnail1__link" data-img="http://placehold.jp/500x500.png">
					<div class="c-thumbnail1__img2" style="background-image: url('http://placehold.jp/500x500.png')"></div>
				</a>
				<a href="#" class="selected c-thumbnail1__link" data-img="http://placehold.jp/500x500.png">
					<div class="c-thumbnail1__img2" style="background-image: url('http://placehold.jp/500x500.png')"></div>
				</a>
				<a href="#" class="selected c-thumbnail1__link" data-img="http://placehold.jp/500x500.png">
					<div class="c-thumbnail1__img2" style="background-image: url('http://placehold.jp/500x500.png')"></div>
				</a>
				<a href="#" class="selected c-thumbnail1__link" data-img="http://placehold.jp/500x500.png">
					<div class="c-thumbnail1__img2" style="background-image: url('http://placehold.jp/500x500.png')"></div>
				</a>
				<a href="#" class="selected c-thumbnail1__link" data-img="http://placehold.jp/500x500.png">
					<div class="c-thumbnail1__img2" style="background-image: url('http://placehold.jp/500x500.png')"></div>
				</a>
				<a href="#" class="selected c-thumbnail1__link" data-img="http://placehold.jp/500x500.png">
					<div class="c-thumbnail1__img2" style="background-image: url('http://placehold.jp/500x500.png')"></div>
				</a>
				<a href="#" class="selected c-thumbnail1__link" data-img="http://placehold.jp/500x500.png">
					<div class="c-thumbnail1__img2" style="background-image: url('http://placehold.jp/500x500.png')"></div>
				</a>
				<a href="#" class="selected c-thumbnail1__link" data-img="http://placehold.jp/500x500.png">
					<div class="c-thumbnail1__img2" style="background-image: url('http://placehold.jp/500x500.png')"></div>
				</a>
				<a href="#" class="selected c-thumbnail1__link" data-img="http://placehold.jp/500x500.png">
					<div class="c-thumbnail1__img2" style="background-image: url('http://placehold.jp/500x500.png')"></div>
				</a>
			</div>
		</div>
	</div>
</div>
