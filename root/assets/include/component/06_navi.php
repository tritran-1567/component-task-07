<?php /*========================================
navi
================================================*/ ?>
<div class="c-dev-title1">navi</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-navi1</div>
<div class="c-navi1">
	<div class="l-container">
		<div class="c-navi1__wrap">
			<ul class="c-navi1__menu">
				<li><a href="">top</a></li>
				<li><a href="">news</a></li>
				<li><a href="">recommend</a></li>
				<li><a href="">item</a></li>
				<li><a href="">shop list</a></li>
				<li><a href="">recruit</a></li>
			</ul>
			<div class="c-navi1__social">
				<a href=""><img src="/assets/img/common/cou-img-8.png" width="130" alt=""></a>
				<a href=""><img src="/assets/img/common/cou-img-9.png" width="22" alt=""></a>
				<a href=""><img src="/assets/img/common/cou-img-10.png" width="20" alt=""></a>
				<a href=""><img src="/assets/img/common/cou-img-11.png" width="20" alt=""></a>
				<a href=""><img src="/assets/img/common/cou-img-12.png" width="20" alt=""></a>
				<a href=""><img src="/assets/img/common/cou-img-13.png" width="20" alt=""></a>
			</div>
		</div>
	</div>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-navi2</div>
<div class="l-container" style="background: #f48eb1">
	<ul class="c-navi2">
		<li><a href="">トップ</a></li>
		<li><a href="">商品情報</a></li>
		<li><a href="">店舗情報</a></li>
		<li><a href="">オンラインショップ</a></li>
		<li><a href="">よくあるご質問</a></li>
		<li><a href="">採用情報</a></li>
		<li><a href="">会社概要</a></li>
		<li><a href="">お問い合わせ</a></li>
	</ul>
</div>
