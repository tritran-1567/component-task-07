<footer class="c-footer">
    <div class="c-footer__wrap">
        <div class="l-container">
            <ul class="c-navi2">
                <li><a href="">トップ</a></li>
                <li><a href="">商品情報</a></li>
                <li><a href="">店舗情報</a></li>
                <li><a href="">オンラインショップ</a></li>
                <li><a href="">よくあるご質問</a></li>
                <li><a href="">採用情報</a></li>
                <li><a href="">会社概要</a></li>
                <li><a href="">お問い合わせ</a></li>
            </ul>
            <ul class="c-footer__social">
                <li><a href=""><img src="/assets/img/common/cou-img-10.png" width="20" alt=""></a></li>
                <li><a href=""><img src="/assets/img/common/cou-img-11.png" width="20" alt=""></a></li>
                <li><a href=""><img src="/assets/img/common/cou-img-12.png" width="20" alt=""></a></li>
                <li><a href=""><img src="/assets/img/common/cou-img-13.png" width="20" alt=""></a></li>
            </ul>
        </div>
    </div>
    <p class="c-footer__copyright">Copyright(C)2016 CouCou All rights reserved.</p>
</footer>
<script src="/assets/js/functions.min.js"></script>
</body>

</html>
