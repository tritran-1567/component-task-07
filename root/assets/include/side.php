<?php /*========================================
side
================================================*/ ?>
<div class="c-dev-title1">side</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-side1</div>
<div class="l-side">
	<aside class="c-side1">
		<div class="c-side1__ttl1">
			<h3>category</h3>
		</div>
		<div class="c-side1__scroll">
			<ul class="c-side1__list">
				<li class="is-active"><a href="">すべて</a></li>
				<li><a href="">キャンペーン</a></li>
				<li><a href="">今月の新商品</a></li>
				<li><a href="">ショップ</a></li>
				<li><a href="">メディア</a></li>
				<li><a href="">お知らせ</a></li>
				<li><a href="">その他</a></li>
				<li></li>
			</ul>
			<div class="c-btn1 c-btn1--c1">
				<a href="">
					<span>news top</span>
				</a>
			</div>
		</div>
	</aside>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-side2</div>
<div class="l-side">
	<aside class="c-side2">
		<div class="c-side2__search pc-only">
			<form action>
                <div class="c-side2__btn">
                    <button type="submit"><img src="/assets/img/common/cou-img-22.png" width="13" alt=""></button>
                </div>
                <input type="text" name="search" placeholder="フリーワード検索">
            </form>
        </div>
        <div class="c-side1__ttl1 sp-only">
			<h3>category</h3>
		</div>
		<div class="c-side2__scroll">
			<ul class="c-side2__list">
				<li><a href="">すべて</a></li>
				<li><a href="">新商品</a></li>
				<li><a href="">オリジナル</a></li>
				<li><a href="">300円以外</a></li>
				<li>
                    <a href="">食品</a>
                    <ul>
                        <li><a href="">菓子</a></li>
                    </ul>
                </li>
				<li>
                    <a href="">リビング</a>
                    <ul>
                        <li><a href="">インテリア小物</a></li>
                        <li><a href="">収納</a></li>
                        <li><a href="">ファブリック</a></li>
                    </ul>
                </li>
				<li>
                    <a href="">キッチン</a>
                    <ul>
                        <li><a href="">食器・カトラリー</a></li>
                        <li><a href="">キッチンツール</a></li>
                        <li><a href="">キッチンその他</a></li>
                    </ul>
                </li>
				<li>
                    <a href="">バス・ランドリー</a>
                    <ul>
                        <li><a href="">バス</a></li>
                        <li><a href="">トイレタリー</a></li>
                        <li><a href="">ランドリー</a></li>
                        <li><a href="">芳香剤</a></li>
                    </ul>
                </li>
				<li>
                    <a href="">ファッション</a>
                    <ul>
                        <li><a href="">アクセサリー</a></li>
                        <li><a href="">ファッション小物</a></li>
                        <li><a href="">フレグランス・ネイル</a></li>
                    </ul>
                </li>
				<li>
                    <a href="">キッズ・ベビー</a>
                </li>
                <li>
                    <a href="">フラワー・ガーデン</a>
                    <ul>
                        <li><a href="">グリーン</a></li>
                        <li><a href="">花器・その他</a></li>
                    </ul>
                </li>
                <li>
                    <a href="">その他</a>
                    <ul>
                        <li><a href="">レイングッズ</a></li>
                    </ul>
                </li>
			</ul>
		</div>
	</aside>
</div>
