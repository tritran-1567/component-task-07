$(document).ready(function () {
	// slide
	$(".c-slide1").slick({
		autoplay: true,
		autoplaySpeed: 2000,
		centerMode: true,
		infinite: true,
		centerPadding: '200px',
		slidesToShow: 1,
		speed: 500,
		variableWidth: false,
		dots: true,
		responsive: [{
			breakpoint: 767,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				centerMode: false,
				arrows: false
			}
		}, ]
	});
	// resize content side hide/show
	$(window).resize(function () {
		if ($(this).width() < 768) {
			$('.c-side1__scroll').hide();
			$('.c-side2__scroll').hide();
		} else {
			$('.c-side1__scroll').show();
			$('.c-side2__scroll').show();
		}
	})
	// Slide up/down content sidebar
	if ($(window).width() < 768) {
		$('.c-side1__scroll').hide();
		$('.c-side2__scroll').hide();
		$('.c-side1__ttl1').click(function () {
			$(this).next().slideToggle(300);
			$(this).toggleClass("is-active");
		});
	} else {
		$('.c-side1__ttl1').off("click");
	}
	// Slide up/down content FAQ
	$('.c-box1__answer').hide();
	$('.is-openfaq').show();
	$('.c-box1__question').click(function () {
		$(this).next().slideToggle(300);
		$(this).toggleClass('is-active');
	});

	//  Height balance
	$(function () {
		$('.c-box3__imgText').matchHeight(false);
	});

	// Click img
	$('.c-thumbnail1__link').on('click', function (e) {
		e.preventDefault();
		var clicked = $(this);
		var newSelection = clicked.data('img');
		var $img = $('.c-thumbnail1__img1').css("background-image", "url(" + newSelection + ")");
		clicked.parent().find('.c-thumbnail1__link').removeClass('selected');
		clicked.addClass('selected');
		$('.c-thumbnail1__img1').empty().append($img.show());
	});
})
